% -*- mode: prolog -*-
% This is your main file

:- module(seven, [seven/1]).

:- use_module(library(six/six), [six/1]).

seven(X) :- six(Y), X is Y + 1.

seven_test :- seven(X), (X=7 -> write("Yes") ; write("No")), nl.
