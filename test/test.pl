% -*- mode: prolog -*-

:- use_module(src(main), [count/2]).

check(true, X, Y) :- (count(X,Y) -> write("It worked!") ; write("It didnt work!")), nl.
check(false, X, Y) :- (count(X,Y) -> write("It didnt work!") ; write("It worked!")), nl.

test_me :- check(true, [1], 1), check(false, [1, 2, 3], 2).
