(defproject seven "0.1.0"
  :type program
  :license "NONE"
  :url "http://example.com/seven"
  :run
   {test-1 {:file [src "seven.pl"], :goal seven/seven_test, :args? false}}
  :vendors [swipl]
  :dependencies [[gitlab.Shillah/six "88499508"]
                 [gitlab.Shillah/five "fe06eca6"]])
